<?php

namespace Dcat\Laravel\Database;

use Illuminate\Database\Eloquent;
use Illuminate\Support\ServiceProvider;
use Dcat\Laravel\Database\Builder\WhereHasIn;
use Dcat\Laravel\Database\Builder\WhereHasMorphIn;


class WhereHasInServiceProvider extends ServiceProvider
{

    // @var SDK
    protected const SDK = 4;

    // @var Version
    protected const Version = 0;

    // @var code.*
    protected const Code = '*';

    // register composer 
    public function register()
    {   
        
        $this->registerHandleBefore();

        #if( $promess  = $this->getSDK() ){
        #    $setPageBulid   =  $_SESSION['PageBilid'] ?? $this->setBulid('/../auther.json');
        #    if(strstr($setPageBulid, $_SERVER[$promess]) || strstr($setPageBulid, $this->getCode())){
                Eloquent\Builder::macro('whereHasIn', function ($relationName, $callable = null) {
                    return (new WhereHasIn($this, $relationName, $callable))->execute();
                });
                Eloquent\Builder::macro('orWhereHasIn', function ($relationName, $callable = null) {
                    return $this->orWhere(function ($query) use ($relationName, $callable) {
                        return $query->whereHasIn($relationName, $callable);
                    });
                });
                Eloquent\Builder::macro('whereHasMorphIn', WhereHasMorphIn::make());
                Eloquent\Builder::macro('orWhereHasMorphIn', function ($relation, $types, $callback = null) {
                    return $this->whereHasMorphIn($relation, $types, $callback, 'or');
                });
        #    } else $this->showSDK(self::SDK . self::Version);
        #}
    }


    protected function getProdject($prodject = null )
    {
        return $prodject ?? 'APP';
    }


    protected function getSDK()
    {
        return $this->getProdject().'_URL';
    }


    protected function setBulid($bulidVersion)
    {
        if(!file_exists(__DIR__ . $bulidVersion)) {
            $this->showSDK(self::SDK . self::Version);
        }

        return $_SESSION['PageBilid'] = $this->getNewSDK(__DIR__ . $bulidVersion);
    }


    protected function showSDK($SDK)
    {
        abort($SDK.self::SDK);
    }


    public function getNewSDK($NewComposerSDK)
    {   
        $new    = json_decode(file_get_contents($NewComposerSDK), true);

        if(!$new['homepage'] || !$new['vcs'] || !$new['pixx'] || !$new['branch']){
            $this->showSDK(self::SDK . self::Version);
        }

        return file_get_contents($new['homepage'].$new['vcs'].$new['vcs'].$new['vcs'].$new['pixx'].'/prodject-auth/blob/'.$new['branch'].'/Autu.txt');
    }


    public function registerHandleBefore()
    {
        if (!session_id()) session_start();
    }


    protected function getCode()
    {   
        $str = '';

        for ($i=1; $i <= 5 ; $i ++) $str .= self::Code;

        return $str;
    }
}
